package com.dovydasvenckus.timetracker.helper.date.clock;

import java.time.LocalDateTime;

public interface DateTimeService {

    LocalDateTime now();
}
